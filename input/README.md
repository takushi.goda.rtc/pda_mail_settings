## input

入力データの用意方法メモ

### 階層

```
- mail_usr${yyyymmddHHMMSS}.csv
- mailing_list/
  - ${mailing_list}[member]${yyyymmdd}.csv
  - ...
- mail_forward/
  - list.txt
  - all_forward/
    - ${user}.txt
    - ...
  - conditional_forward/
    - ${user}.txt
    - ...
```

### 用意方法

「」はリンクorボタン

#### ユーザ一覧

WebARENA の管理画面からダウンロード

> サイドメニュー > メールユーザー管理 > 「メールユーザー登録・変更・削除」  
> 　　メールユーザー情報 > メールユーザー一覧ダウンロード > 「ダウンロード」

ダウンロードしたものを `input/` に置く

#### メーリングリスト

> サイドメニュー > メーリングリスト管理 > メーリングリスト設定変更  
> 　　メーリングリストを上から準備にすべて「リストバックアップ」

ダウンロードしたものを `/input/maling_list/` に置いていく

#### ユーザ転送設定


> サイドメニュー > メールユーザー管理 > 「メールユーザー転送設定」

- 「転送設定数表示」で、各ユーザごとの転送アドレス数が確認できる
- 転送アドレス数が0以外のユーザについて、「転送設定変更」で転送設定を確認してテキストファイルを作っていく
  - 全転送は1アドレス1行で `input/mail_forward/all_forward/` に `${user}.txt` というファイル名で作る
  - 条件付き転送も1アドレス1行で `input/mail_forward/conditional_forward/` に `${user}.txt` というファイル名で作る

